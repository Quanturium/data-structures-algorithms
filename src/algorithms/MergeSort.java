package algorithms;

/**
 * @Description Java implementation of the MergeSort algorithm
 */
public class MergeSort
{
	private int[] array;
	private int[] temp;

	public static void main(String[] args)
	{
		int[] unsortedArray = new int[]{5,6,8,7,0,1,9,4,2,3};

		MergeSort mergeSort = new MergeSort(unsortedArray);
		mergeSort.print();
		mergeSort.run();
		mergeSort.print();
	}

	MergeSort(int[] array)
	{
		this.array = array;
		this.temp = new int[array.length];
	}

	public void run()
	{
		this.divide(0, this.array.length-1);
	}

	private void divide(int low, int high)
	{
		if(high - low < 1)
			return;

		int middle = (low + high) / 2;

		this.divide(low, middle);
		this.divide(middle+1,high);

		this.merge(low, middle, high);
	}

	private void merge(int low, int middle, int high)
	{
		int nl = low, nh = middle+1;
		int i = low;

		while(nl <= middle && nh <= high)
		{
			if(this.array[nl] < this.array[nh])
			{
				this.temp[i] = this.array[nl];
				nl++;
			}
			else
			{
				this.temp[i] = this.array[nh];
				nh++;
			}

			i++;
		}

		while(nl <= middle)
		{
			this.temp[i] = this.array[nl];
			nl++;
			i++;
		}

		while(nh <= high)
		{
			this.temp[i] = this.array[nh];
			nh++;
			i++;
		}

		for(i = low ; i <= high ; i++)
		{
			this.array[i] = this.temp[i];
		}
	}

	public void print()
	{
		for(int i = 0 ; i < this.array.length ; i++)
			System.out.print(this.array[i]);

		System.out.println();
	}
}
