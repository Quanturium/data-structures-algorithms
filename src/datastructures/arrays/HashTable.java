package datastructures.arrays;

/**
 * @description Implementation of an HashTable. This code can be easily edited to create larger hashTables. They key is an integer, the
 * value can be of any type (Thanks to generics). The implementation uses close addressing with a LinkedList.
 * The methods are get(key), put(key, value), remove(key)
 */

public class HashTable<V>
{
	private final static int TABLE_SIZE = 64;
	private HashEntry[] table;

	HashTable()
	{
		this.table = new HashEntry[TABLE_SIZE];

		for (int i = 0; i < TABLE_SIZE; i++)
			this.table[i] = null;
	}

	public V get(int key)
	{
		int hashKey = key % TABLE_SIZE;
		HashEntry entry = table[hashKey];

		while(entry != null && entry.getKey() != key)
			entry = entry.getNext();

		return (entry == null) ? null : (V) entry.getValue();
	}

	public void put(int key, V value)
	{
		int hashKey = key % TABLE_SIZE;
		HashEntry entry = table[hashKey];
		HashEntry newEntry = new HashEntry(key,value);

		if(entry == null)
		{
			table[hashKey] = newEntry;
		}
		else
		{
			while(entry != null && entry.getKey() != key)
				entry = entry.getNext();

			if(entry.getKey() == key)
				entry.setValue(value);
			else
				entry.setNext(newEntry);
		}
	}

	public void remove(int key)
	{
		int hashKey = key % TABLE_SIZE;
		HashEntry entry = table[hashKey];
		HashEntry prevEntry = null;

		if(entry != null)
		{
			while(entry != null && entry.getKey() != key)
			{
				prevEntry = entry;
				entry = entry.getNext();
			}

			if(entry.getKey() == key)
			{
				if(prevEntry == null)
					this.table[hashKey] = entry.getNext();
				else
					prevEntry.setNext(entry.getNext());
			}
		}
	}

	public class HashEntry<V>
	{
		private int key;
		private V value;
		private HashEntry next;

		HashEntry(int key, V value)
		{
			this.key = key;
			this.value = value;
		}

		public int getKey()
		{
			return this.key;
		}

		public V getValue()
		{
			return this.value;
		}

		public void setValue(V value)
		{
			this.value = value;
		}

		public HashEntry getNext()
		{
			return this.next;
		}

		public void setNext(HashEntry next)
		{
			this.next = next;
		}
	}
}

