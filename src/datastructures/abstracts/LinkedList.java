package datastructures.abstracts;

/**
 * @description Implementation of a basic LinkedList with addLast(value), addFirst(value), removeLast(), removeFirst() methods
 * The list accepts any value. Work in progress.
 */

public class LinkedList<T>
{
	private LinkedListNode<T> head;
	private LinkedListNode<T> tail;

	public void addLast(T value)
	{
		if(value == null)
			return;

		LinkedListNode<T> newNode = new LinkedListNode<T>(value);

		if(this.head == null)
		{
			this.head = newNode;
			this.tail = newNode;
		}
		else
		{
			this.tail.next = newNode;
			this.tail = newNode;
		}
	}

	public void addFirst(T value)
	{
		if(value == null)
			return;

		LinkedListNode<T> newNode = new LinkedListNode<T>(value);

		if(this.head == null)
		{
			this.head = newNode;
			this.tail = newNode;
		}
		else
		{
			newNode.next = this.head;
			this.head = newNode;
		}
	}

	public void removeFirst()
	{
		if(this.head == null)
			return;
		if(this.head == this.tail)
		{
			this.head = null;
			this.tail = null;
		}
		else
			this.head = this.head.next;
	}

	public void removeLast()
	{
		if(this.tail == null)
			return;
		else if(this.head == this.tail)
		{
			this.head = null;
			this.tail = null;
		}
		else
		{
			LinkedListNode<T> current = this.head;

			while(current.next != this.tail)
			{
				current = current.next;
			}

			tail = current;
			current.next = null;
		}
	}

	public class LinkedListNode<T>
	{
		private T value;
		public LinkedListNode<T> next;

		LinkedListNode(T value)
		{
			this.value = value;
			this.next = null;
		}

		public T getValue()
		{
			return this.value;
		}

		public void setValue(T value)
		{
			this.value = value;
		}
	}
}
