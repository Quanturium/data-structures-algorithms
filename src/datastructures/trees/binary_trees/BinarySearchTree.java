package datastructures.trees.binary_trees;

/**
 * @Description An Implementation of a BST with a value of any type. The type (or object) has to implement the Comparable interface.
 * The methods are insert(value). Work in progress.
 */

public class BinarySearchTree<E extends Comparable<E>>
{
	public Node<E> root;

	public void insert(E value)
	{
		this.root = this.insert(value, root);
	}

	private Node<E> insert(E value, Node<E> node)
	{
		if (node == null)
			node = new Node<E>(value);
		else if (value.compareTo(node.value) < 0)
			node.left = insert(value, node.left);
		else if (value.compareTo(node.value) > 0)
			node.right = insert(value, node.right);

		return node;
	}

	public class Node<E extends Comparable<E>>
	{
		public Node<E> left;
		public Node<E> right;
		public E value;

		Node(E value)
		{
			this.value = value;
		}
	}
}
