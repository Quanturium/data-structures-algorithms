package datastructures.trees.heaps;

/**
 * @description Implementation of a BinaryHeap (min) for value integers. The available methods are getMin(), isEmpty(), insert(int), removeMin().
 * This datastructure is often use as a PriorityQueue. Work in progress.
 */


public class BinaryMinHeap
{
	private int[] data;
	private int size;

	BinaryMinHeap(int size)
	{
		this.data = new int[size];
		this.size = 0;
	}

	public int getMin() throws Exception
	{
		if (isEmpty())
			throw new Exception("The Heap is empty");
		else
			return data[0];
	}

	public boolean isEmpty()
	{
		return (this.size == 0);
	}

	public void insert(int value) throws Exception
	{
		if (this.data.length == size)
			throw new Exception("The size of the heap has been reached");
		else
		{
			this.size++;
			data[size - 1] = value;

		}
	}

	public void removeMin() throws Exception
	{
		if (isEmpty())
			throw new Exception("The heap is empty");
		else
		{
			data[0] = data[size - 1];
			size--;
			if (size > 0)
				siftDown(0);
		}
	}

	protected void siftUp(int nodeIndex)
	{
		int parentIndex, tmp;

		if (nodeIndex != 0)
		{
			parentIndex = getParentIndex(nodeIndex);

			if (data[parentIndex] > data[nodeIndex])
			{
				tmp = data[parentIndex];
				data[parentIndex] = data[nodeIndex];
				data[nodeIndex] = tmp;
				siftUp(parentIndex);
			}
		}
	}

	protected void siftDown(int nodeIndex)
	{
		int leftChildIndex, rightChildIndex, minIndex, tmp;

		leftChildIndex = getLeftChildIndex(nodeIndex);
		rightChildIndex = getRightChildIndex(nodeIndex);

		if (rightChildIndex >= size)
		{
			if (leftChildIndex >= size)
				return;
			else
				minIndex = leftChildIndex;
		}
		else
		{
			if (data[leftChildIndex] <= data[rightChildIndex])
				minIndex = leftChildIndex;
			else
				minIndex = rightChildIndex;
		}

		if (data[nodeIndex] > data[minIndex])
		{
			tmp = data[minIndex];
			data[minIndex] = data[nodeIndex];
			data[nodeIndex] = tmp;
			siftDown(minIndex);
		}
	}

	protected int getLeftChildIndex(int nodeIndex)
	{
		return 2 * nodeIndex + 1;
	}

	protected int getRightChildIndex(int nodeIndex)
	{
		return 2 * nodeIndex + 2;
	}

	protected int getParentIndex(int nodeIndex)
	{
		return (nodeIndex - 1) / 2;
	}
}
