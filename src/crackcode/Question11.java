package crackcode;

/**
 * @Question Implement an algorithm to determine if a string has all unique characters. What if you can not use additional data structures?
 * @Solution I assume the chars are ASCII and only lowercases 'a' to 'z'. We have a total of 26 chars. An Integer is 32-1
 * (an integer in java is signed) so we can use a simple integer with bitwise operations instead of allocating a big array of booleans.
 * That way we reduced the space complexity to O(1). The time complexity is still O(n).
 */

public class Question11
{
	public static void main(String[] args)
	{
		String[] datas = new String[4];
		datas[0] = "";
		datas[1] = "abc";
		datas[2] = "abac";
		datas[3] = "abcdefghijklmnopqrstuvwxyz";

		for(String data : datas)
		{
			boolean result = run(data);
			System.out.println(data + " => " + result);
		}
	}

	public static boolean run(String word)
	{
		int flag = 0;

		for(int i = 0 ; i < word.length() ; i++)
		{
			int val = word.charAt(i) - 'a'; // So that 'a' = 0, and 'z' = 25. See ASCII table

			if((flag & (1 << val)) > 0) // Binary AND. If the result is > 0 it means we already found the char at a previous iteration
				return false;

			// We flag the char
			flag ^= 1 << val;
		}

		return true;
	}
}
