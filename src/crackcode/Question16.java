package crackcode;

/**
 * @Question Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the
 * image by 90 degrees. Can you do this in place?
 * @Solution The method run(matrix, n) below takes a matrix of size n and performs a 90 degrees clockwise rotation of the matrix's data.
 * The time complexity of the solution is O(n), the space complexity is O(1)
 */

public class Question16
{
	public static void main(String[] args)
	{
		int n = 6;
		int[][] matrix = new int[n][n];

		for(int i = 0 ; i < n ; ++i)
		{
			for(int j = 0 ; j < n ; j++)
			{
				matrix[i][j] = 0;
			}
		}

		matrix[0][0] = 1;
		matrix[1][0] = 1;
		matrix[2][0] = 1;
		matrix[3][0] = 1;
		matrix[4][0] = 1;
		matrix[1][1] = 1;

		print(matrix,n);
		run(matrix,n);
		print(matrix,n);
	}

	public static void run(int[][]matrix, int size)
	{
		for(int i = 0 ; i < size/2 ; i++)
		{
			int start = i;
			int end = size - 1 - i;

			for(int j = start ; j < end ; j++)
			{
			 	int temp = matrix[start][j]; // top

				// left to top
				matrix[start][j] = matrix[end - j][start];

				// bottom to left
				matrix[end - j][start] = matrix[end][end - j];

				// right to bottom
				matrix[end][end - j] = matrix[j][end];

				// top to right
				matrix[j][end] = temp;
			}
		}
	}

	public static void print(int[][]matrix, int size)
	{
		for(int i = 0 ; i < size ; i++)
		{
			String out = "";
			for(int j = 0 ; j < size ; j++)
			{
				out += matrix[i][j];
			}

			System.out.println(out);
		}

		System.out.println();
	}
}
