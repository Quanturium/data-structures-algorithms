package crackcode;

/**
 * @Question Implement an algorithm to print all valid (e.g., properly opened and closed) combinations of n-pairs of parentheses.
 */

public class Question85
{
	public static void main(String[] args)
	{
		int number = 2;
		char[] data = new char[number*2];

		run(data, number, number, 0);
	}

	public static void run(char[] data, int l, int r, int index)
	{
		if(l == 0 && r == 0)
			System.out.println(data);

		if(l > 0)
		{
			data[index] = '(';
			run(data, l-1, r, index+1);
		}

		if(r > l)
		{
			data[index] = ')';
			run(data, l, r-1, index+1);
		}
	}
}
