package crackcode;

/**
 * @Question A sorted array has been rotated so that the elements might appear in the order 3 4 5 6 7 1 2. How would you
 * find the minimum element?
 */
public class Question01
{
	public static void main(String[] args)
	{
		int[] array1 = new int[]{3,4,5,6,7,1,2};
		int[] array2 = new int[]{0,1,2,3,4,5,6,7};
		int[] array3 = new int[]{};
		int[] array4 = null;

		System.out.println(minimumElement(array1));
		System.out.println(minimumElement(array2));
		System.out.println(minimumElement(array3));
		System.out.println(minimumElement(array4));
	}

	public static int minimumElement(int[] array)
	{
		if(array == null || array.length < 1)
			return -1;

		return minimumElementHelper(array, 0, array.length-1);
	}

	private static int minimumElementHelper(int[] array, int minIndex, int maxIndex)
	{
		int middleIndex = (maxIndex-minIndex)/2 + minIndex;

		if(minIndex == middleIndex) // Because the result of (maxIndex-minIndex)/2 is floor()
			return (array[minIndex] < array[maxIndex]) ? array[minIndex] : array[maxIndex];
		if(array[minIndex] > array[middleIndex])
			return minimumElementHelper(array, minIndex, middleIndex);
		else if(array[middleIndex] > array[maxIndex])
			return minimumElementHelper(array, middleIndex, maxIndex);
		else
			return array[minIndex]; // We need this if the array hasn't been rotated
	}
}
