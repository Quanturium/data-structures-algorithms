package codeeval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Question You are given two strings 'A' and 'B'. Print out a 1 if string 'B' occurs at the end of string 'A'. Else a zero.
 */
public class TrailingString
{
	public static void main(String[] args) throws IOException
	{
		FileReader fileReader = new FileReader(args[0]);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line = null;

		while ((line = bufferedReader.readLine()) != null)
		{
			String[] strings = line.split(",");

			if (strings.length != 2)
				continue;

			System.out.println(compareEnds(strings[0], strings[1]) ? "1" : "0");
		}

		bufferedReader.close();
	}

	public static boolean compareEnds(String a, String b)
	{
		int la = a.length();
		int lb = b.length();

		if (la < lb)
			return false;

		for (int i = 1; i <= b.length(); i++)
		{
			if (a.charAt(la - i) != b.charAt(lb - i))
				return false;
		}

		return true;
	}
}
