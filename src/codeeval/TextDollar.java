package codeeval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Question You are given a positive integer number. This represents the sales made that day in your department store. The payables
 * department however, needs this printed out in english. NOTE: The correct spelling of 40 is Forty. (NOT Fourty)
 * @Sample Input: 1234 ; Output: OneThousandTwoHundredThirtyFourDollars
 * @Solution It's very straightforward, It would have been much harder in French :)
 */

public class TextDollar
{
	public static void main(String[] args) throws IOException
	{
		FileReader fileReader = new FileReader(args[0]);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line = null;

		while ((line = bufferedReader.readLine()) != null)
		{
			System.out.println(numberToString(Integer.parseInt(line)) + "Dollars");
		}

		bufferedReader.close();
	}

	public static String numberToString(int number)
	{
		if (number < 20)
		{
			String[] zeroToNineteen = new String[]{"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
			return zeroToNineteen[number];
		}
		else if (number < 100)
		{
			String[] twentyToNinety = new String[]{"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
			return twentyToNinety[number / 10 - 2] + (number % 10 > 0 ? numberToString(number % 10) : "");
		}
		else if (number < 1000)
		{
			return numberToString(number / 100) + "Hundred" + (number % 100 > 0 ? numberToString(number % 100) : "");
		}
		else if (number < 1000000)
		{
			return numberToString(number / 1000) + "Thousand" + (number % 1000 > 0 ? numberToString(number % 1000) : "");
		}
		else
		{
			return numberToString(number / 1000000) + "Million" + (number % 1000000 > 0 ? numberToString(number % 1000000) : "");
		}
	}
}
