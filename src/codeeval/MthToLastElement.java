package codeeval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Question Write a program to determine the Mth to last element of a list.
 * @Sample Input: a b c d 4 ; Output: a
 */

public class MthToLastElement
{
	public static void main(String[] args) throws IOException
	{
		FileReader fileReader = new FileReader(args[0]);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line = null;

		while ((line = bufferedReader.readLine()) != null)
		{
			String[] chars = line.split(" ");
			int index = Integer.valueOf(chars[chars.length - 1]);

			if (chars.length - index > 0)
				System.out.println(chars[chars.length - 1 - index]);
		}

		bufferedReader.close();
	}
}
