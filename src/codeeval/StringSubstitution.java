package codeeval;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Question Given a string S, and a list of strings of positive length, F1,R1,F2,R2,...,FN,RN, proceed to find in order the occurrences
 * (left-to-right) of Fi in S and replace them with Ri. All strings are over alphabet { 0, 1 }. Searching should consider only contiguous
 * pieces of S that have not been subject to replacements on prior iterations. An iteration of the algorithm should not write over any
 * previous replacement by the algorithm.
 * @Sample Input: 10011011001;0110,1001,1001,0,10,11 ; output: 11100110
 */

public class StringSubstitution
{
	public static void main(String[] args) throws IOException
	{
		FileReader fileReader = new FileReader(args[0]);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line = null;

		while ((line = bufferedReader.readLine()) != null)
		{
			String[] split1 = line.split(";");
			String[] split2 = split1[1].split(",");

			System.out.println(replace(split1[0], split2, 0));
		}

		bufferedReader.close();
	}

	public static String replace(String str, String[] replacements, int offset)
	{
		if (replacements.length > offset)
		{
			int p = -1;

			if ((p = str.indexOf(replacements[offset])) != -1)
				return replace(str.substring(0, p), replacements, offset) + replacements[offset + 1] + replace(str.substring(p + replacements[offset].length(), str.length()), replacements, offset);
			else
				return replace(str, replacements, offset + 2);
		}

		return str;
	}
}
