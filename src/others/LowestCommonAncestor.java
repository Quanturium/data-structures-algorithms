package others;

import datastructures.trees.binary_trees.BinarySearchTree;

/**
 * @Question Find the lowest common ancestor of 2 values in an BST
 * @Answer We go through each node recursively and we return the node when it matches one of the 2 values. We'll get the LCA when
 * we get 2 non-null nodes.
 */
public class LowestCommonAncestor
{
	public static void main(String[] args)
	{
		BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();

		bst.insert(7);
		bst.insert(3);
		bst.insert(9);
		bst.insert(5);
		bst.insert(1);

		BinarySearchTree.Node result = LCA(bst, 5, 0);
		System.out.print(result != null ? result.value : "no result");
	}

	public static BinarySearchTree.Node LCA(BinarySearchTree<Integer> bst, Integer a, Integer b)
	{
		return LCAHelper(bst.root, a, b);
	}

	public static BinarySearchTree.Node LCAHelper(BinarySearchTree.Node node, Integer a, Integer b)
	{
		if(node == null)
			return null;

		if(node.value == a || node.value == b)
			return node;
		else
		{
			BinarySearchTree.Node l,r;
			l = LCAHelper(node.left, a, b);
			r = LCAHelper(node.right, a, b);

			if(l != null && r != null)
				return node;
			else
				return (l != null) ? l : r;
		}
	}
}
