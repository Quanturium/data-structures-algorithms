package others;

import sun.rmi.rmic.Main;

/**
 * @Description A simple algorithm solving the Towers of Hanoi game. It just prints out the movements but doesn't actually change any data.
 * It simply uses the power of recursion. Work in progress.
 */

public class TowersOfHanoi
{
	private final int nbOfDisks;

	public static void main(String[] args)
	{
		new TowersOfHanoi(3).run();
	}

	public TowersOfHanoi(int nbOfDisks)
	{
		this.nbOfDisks = nbOfDisks;
	}

	public void run()
	{
		MoveTower(this.nbOfDisks, 1, 2, 3);
	}

	private void MoveTower(int diskId, int source, int destination, int spare)
	{
		if (diskId == 0)
		{
			System.out.println("Moving disk 0 : " + source + " -> " + destination);
		}
		else
		{
			MoveTower(diskId - 1, source, spare, destination);
			System.out.println("Moving disk " + diskId + " : " + source + " -> " + destination);
			MoveTower(diskId - 1, spare, destination, source);
		}
	}
}
