package others;

/**
 * @Description This algorithm uses backtracking to brute-force the sudoku game. It goes from left to right, top to bottom, and each time a
 * number is invalid, it backtracks until a valid number can be placed.
 */

public class Sudoku
{
	private int[][] grid;

	public static void main(String[] args)
	{
		int[][] grid = new int[9][9];

		grid[0] = new int[]{5, 3, 0, 0, 7, 0, 0, 0, 0};
		grid[1] = new int[]{6, 0, 0, 1, 9, 5, 0, 0, 0};
		grid[2] = new int[]{0, 9, 8, 0, 0, 0, 0, 6, 0};
		grid[3] = new int[]{8, 0, 0, 0, 6, 0, 0, 0, 3};
		grid[4] = new int[]{4, 0, 0, 8, 0, 3, 0, 0, 1};
		grid[5] = new int[]{7, 0, 0, 0, 2, 0, 0, 0, 6};
		grid[6] = new int[]{0, 6, 0, 0, 0, 0, 2, 8, 0};
		grid[7] = new int[]{0, 0, 0, 4, 1, 9, 0, 0, 5};
		grid[8] = new int[]{0, 0, 0, 0, 8, 0, 0, 7, 9};

		new Sudoku(grid).run();
	}

	public Sudoku(int[][] grid)
	{
		this.grid = grid;
	}

	public void run()
	{
		try
		{
			print();
			solve(0, 0);
		} catch (Exception e)
		{
			if (e.getMessage().equals("end"))
				print();
		}
	}

	private void solve(int x, int y) throws Exception
	{
		if (grid[x][y] != 0)
		{
			next(x, y);
		}
		else
		{
			for (int i = 1; i <= 9; i++)
			{
				grid[x][y] = i;

				if (isValidCell(x, y) && isValidCol(x, y) && isValidRow(x, y))
					next(x, y);

				grid[x][y] = 0;
			}
		}
	}

	private void next(int x, int y) throws Exception
	{
		if (y == 8)
		{
			if (x == 8)
				throw new Exception("end");
			else
				solve(x + 1, 0);
		}
		else
		{
			solve(x, y + 1);
		}
	}

	private boolean isValidRow(int x, int y)
	{
		for (int i = 0; i < 9; i++)
		{
			if (grid[x][i] == grid[x][y] && y != i)
				return false;
		}

		return true;
	}

	private boolean isValidCol(int x, int y)
	{
		for (int i = 0; i < 9; i++)
		{
			if (grid[i][y] == grid[x][y] && x != i)
				return false;
		}

		return true;
	}

	private boolean isValidCell(int x, int y)
	{
		int a = (x / 3) * 3;
		int b = (y / 3) * 3;

		for (int i = a; i < a + 2; i++)
		{
			for (int j = b; j < b + 2; j++)
			{
				if (grid[i][j] == grid[x][y] && i != x && j != y)
					return false;
			}
		}

		return true;
	}

	private void print()
	{
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				System.out.print(" " + grid[i][j] + " ");

				if ((j + 1) % 3 == 0 && (j + 1) < 9)
					System.out.print("|");
			}

			System.out.println();

			if ((i + 1) % 3 == 0 && i + 1 < 9)
				System.out.println("----------------------------");
		}

		System.out.println();
		System.out.println();
	}
}
