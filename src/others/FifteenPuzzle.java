package others;

/**
 * @Description To solve a Fifteen puzzle, we can use th A* algorithm with an heuristic function. The heuristic function would be the
 * sum of the distance between misplaced tile positions and their real position. Work in progress.
 */

public class FifteenPuzzle
{

	private int[][] grid;

	public static void main(String[] args)
	{
		int[][] grid = new int[4][4];

		grid[0] = new int[]{1, 2, 3, 4};
		grid[1] = new int[]{5, 6, 7, 8};
		grid[2] = new int[]{9, 10, 11, 12};
		grid[3] = new int[]{13, 14, 15, 0};

		new FifteenPuzzle(grid).run();
	}

	public FifteenPuzzle(int[][] grid)
	{
		this.grid = grid;
	}

	public void run()
	{
		print();
	}

	private void print()
	{
		System.out.println("-----------------");

		for (int i = 0; i < 4; i++)
		{
			System.out.print("|");

			for (int j = 0; j < 4; j++)
			{
				System.out.print(String.format("%3d", grid[i][j]));
				System.out.print("|");
			}

			System.out.println();

		}

		System.out.println("-----------------");
		System.out.println();
	}
}
